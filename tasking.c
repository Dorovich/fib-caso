#include <mach/mach.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define PGSIZE (16*1024)
int main (int argc, char *argv[])
{
        vm_address_t addr = 0x110000000;
        task_t child;
        int pid = fork();
        if (pid != 0) {
                fprintf(stdout, "pid padre %d\n", getpid());
                fprintf(stdout, "pid hijo %d\n", pid);
                int res = task_for_pid(mach_task_self(), pid, &child);
                if (res != KERN_SUCCESS) {
                        fprintf(stderr, "task_for_pid: %s\n", mach_error_string(res));
                        exit(1);
                }
                res = vm_allocate(child, &addr, PGSIZE, FALSE);
                if (res != KERN_SUCCESS) {
                        fprintf(stderr, "vm_allocate: %s\n", mach_error_string(res));
                        exit(1);
                }
                char data[PGSIZE/sizeof(char)] = {0};
                data[0] = 'A';
                data[1] = 'b';
                data[2] = '\n';
                res = vm_write(child, addr, (vm_offset_t)data, PGSIZE);
                if (res != KERN_SUCCESS) {
                        fprintf(stderr, "vm_write: %s\n", mach_error_string(res));
                        exit(1);
                }
                sleep(60);
                //task_terminate(child);
                //fprintf(stdout, "hijo matado\n");
        } else {

                fprintf(stdout, "leo un %d\n", ((int *)addr)[0]);
                fprintf(stdout, "leo un %d\n", ((int *)addr)[1]);
                sleep(60);
        }
        return 0;
}