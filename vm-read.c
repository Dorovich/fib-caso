#include <mach/mach.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define PGSIZE (16*1024)
#define RDSIZE 256
void usage ()
{
        fprintf(stdout, "Usage: vm-read <pid> <mem_address>\n");
        exit(1);
}
int main (int argc, char *argv[])
{
        if (argc != 3) usage();
        int pid = atoi(argv[1]);
        vm_address_t addr = strtol(argv[2], NULL, 0);
        task_t task;
        int res = task_for_pid(mach_task_self(), pid, &task);
        if (res != KERN_SUCCESS) {
                fprintf(stderr, "task_for_pid: %s\n", mach_error_string(res));
                exit(1);
        }
        vm_offset_t *buffer;
        mach_msg_type_number_t read_bytes;
        res = vm_read(task, addr, RDSIZE, (vm_offset_t *) &buffer, &read_bytes);
        if (res != KERN_SUCCESS) {
                fprintf(stderr, "vm_read: %s\n", mach_error_string(res));
                exit(1);
        }
        res = write(1, buffer, 3 /*read_bytes*/);
        if (res < 0) {
                fprintf(stderr, "write: %s\n", strerror(res));
                exit(1);
        }
        return 0;
}