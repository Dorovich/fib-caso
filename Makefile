all: proc memory-management mps mtask thread0 thread vm vm-read msg tasking

proc: proc.c 
	gcc -D_GNU_SOURCE -g -O -o proc proc.c

memory-management: memory-management.c
	gcc -D_GNU_SOURCE -g -O -o memory-management memory-management.c

mps: mps.c print-task-info.c
	gcc -D_GNU_SOURCE -g -O -o mps mps.c print-task-info.c

mtask: mtask.c print-task-info.c
	gcc -D_GNU_SOURCE -g -O -o mtask mtask.c print-task-info.c

thread0: thread.c
	gcc -D_GNU_SOURCE -g -O0 -o thread0 thread.c

thread: thread.c
	gcc -D_GNU_SOURCE -g -O -o thread thread.c

msg: msg.c
	cc -D_GNU_SOURCE -g -O -o msg msg.c

vm: vm.c
	cc -D_GNU_SOURCE -g -O -o vm vm.c

vm-read: vm-read.c
	cc -D_GNU_SOURCE -g -O -o vm-read vm-read.c

tasking: tasking.c
	cc -D_GNU_SOURCE -g -O -o tasking tasking.c

clean:
	rm -f thread thread.o mps mps.o print-task-info.o \
              msg msg.o vm vm.o vm-read vm-read.o proc proc.o \
              mtask mtask.o
