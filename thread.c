#include <mach/mach.h>
#include <mach/mach_error.h>
#include <mach/thread_status.h>
#include <mach/i386/thread_status.h>
#include <mach/machine/thread_status.h>
#include <stdio.h>
#include <unistd.h>
#include <mach/mach_traps.h>
#include <stdlib.h>
#include <pthread.h>

#include "tsd.h"

char localdata[65536];


// useu _thread_set_tsd_base(localdata+32768) a la funcio del nou thread
// per donar-li espai per les variables thread_local

// useu aquesta funció pel nou thread
//   - poseu-li una variable local i inicialitzeu-la
//   - podeu mirar si fa servir la pila, desenssamblant:
//        $ objdump -d thread      o    $ objdump -d thread0
//   - poseu-li un bucle infinit per veure que consumeix cpu
//       i per evitar que retorni... si retorna... on anirà??? (crash)
//   - poseu-li un write per veure que pot fer crides de UNIX
//   - poseu-li un printf per veure que pot fer crides de libc i 
//       comprovareu que li farà falta tenir el tsd base ben posat
//   - poseu-li un argument, inicialitzeu-lo des del main i comproveu que
//       el rep correctament. Com li podeu passar des del punt on el creeu?
//   - podeu assignar-lo a un core en concret?
//       (busqueu l'affinity API de MacOSX)
void thread_func()
{
	_thread_set_tsd_base(localdata+32768);
	int test = 44;
	printf("test!!!\n");
	while(1);
}

int main() {
	int res;
	size_t cpuid;
	mach_port_t task = mach_task_self();
	mach_port_t newthread;

	res = _os_cpu_number();
	printf ("os_cpu %d\n", res);

	pthread_cpu_number_np(&cpuid);
	printf ("pthread_cpu_number_np %zu\n", cpuid);

	/*OS_GS_RELATIVE*/ void * pb = _os_tsd_get_base();
	printf ("main tsd_base %p\n", pb);

	_STRUCT_ARM_THREAD_STATE64 thread_state;
	mach_msg_type_number_t size = ARM_THREAD_STATE64_COUNT;

	// creeu el thread
	thread_act_t *t, s;
	t = &s;
	res = thread_create(mach_task_self(), t);

	// comproveu errors
	if (res != KERN_SUCCESS) {
		fprintf (stderr, "thread_create: %s\n", mach_error_string(res));
		exit(1);
	}

	// Podeu veure les estructures de context:
	//     /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/
	//           usr/include//mach/arm/_structs.h
	//        -> _STRUCT_ARM_THREAD_STATE64
	//              __x[29];
	//              __fp;
	//              __lr;
	//              __sp;
	//              __pc;
	//              __cpsr;
	//              __pad;
	//           ...
	//
	//     /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/
	//           usr/include//mach/arm/thread_status.h
	//        -> Flavors
	//        -> arm_thread_state64_t
	//           ...

	// agafeu el context del thread
	arm_thread_state64_t ctx;
	res = thread_get_state(*t, ARM_THREAD_STATE64, (thread_state_t)&ctx, &size);

	// comproveu errors
	if (res != KERN_SUCCESS) {
		fprintf(stderr, "thread_get_state: %s\n", mach_error_string(res));
		exit(1);
	}

	// imprimiu els valors dels registres
	int i;
	for (i=0; i < 29; i++) {
		printf ("x%02d: %016llx\n", i, ctx.__x[i]);
	}
	printf ("fp:   %016llx\n", ctx.__fp);
	printf ("lr:   %016llx\n", ctx.__lr);
	printf ("sp:   %016llx\n", ctx.__sp);
	printf ("pc:   %016llx\n", ctx.__pc);
	printf ("cpsr: %08x\n", ctx.__cpsr);
	printf ("pad:  %08x\n", ctx.__pad);

	// demaneu memoria per a la pila del thread (feu-la de 64KB)

	void * memory;
	memory = malloc(64*1024);
	printf ("Stack mem [0x%lx - 0x%lx)\n", (unsigned long) memory,
			(unsigned long) memory + 65536);

	// assigneu el registre __rsp apuntant correctament a la pila

	ctx.__sp = (__uint64_t)&memory[(64*1024)];

	// assigneu el registre __pc apuntant correct. a la funcio del thread

	ctx.__pc = (__uint64_t)thread_func;

	// assigneu el nou context al thread

	res = thread_set_state(*t, ARM_THREAD_STATE64, (thread_state_t)&ctx, size);

	// comproveu errors
	if (res != KERN_SUCCESS) {
		fprintf(stderr, "thread_set_state: %s\n", mach_error_string(res));
		exit(1);
	}

	printf ("pid: %d   localdata %p\n", getpid(), localdata);

	// feu el thread_resume

	res = thread_resume(*t);

	// comproveu errors
	if (res != KERN_SUCCESS) {
		fprintf(stderr, "thread_resume: %s\n", mach_error_string(res));
		exit(1);
	}

	sleep(6);

	return 0;
}
